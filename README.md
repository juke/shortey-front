# [shortey-front](https://codeberg.org/juke/shortey-front)


### Usage

A deployed version is on https://shortey-front.paas.juke.fr

The backend is here https://codeberg.org/juke/shortey

## Development

To clone the repository locally:

```bash
$ git clone https://codeberg.org/juke/shortey-front.git
```

## Contributing

Feel free to contact via the email bellow with a patch or anything.

### Issues
Open new issues by mailing [issues+shortey-front@juke.fr](mailto:issues+shortey-front@juke.fr)

---
beep boop

