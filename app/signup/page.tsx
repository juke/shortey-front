"use client";
import { useState } from "react";
import { useInput } from "../../input";

export default function Signup() {
  const [email, Email] = useInput("email");
  const [password, Password] = useInput("password");
  const [res, setRes] = useState("");

  const signup = async (e: any) => {
    e.preventDefault();
    try {
      const response = await fetch("https://shortey.paas.juke.fr/auth/signup", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email,
          password,
        }),
      });

      if (response.status !== 201) return setRes(await response.text());

      const { access_token } = await response.json();
      localStorage.setItem("access_token", access_token);
      if (access_token) return location.assign("/");
    } catch (err: any) {
      setRes(err.message);
    }
  };
  return (
    <div>
      <div className="flex justify-center">
        <div className="w-full max-w-xs">
          <div className="bg-white dark:bg-gray-900 shadow-md rounded px-8 pt-6 pb-8 mb-4 mt-4">
            <div className="mb-4">
              <label className="block text-gray-700 dark:text-white text-sm font-bold mb-2">
                Email
              </label>
              {Email}
            </div>
            <div className="mb-6">
              <label className="block text-gray-700 dark:text-white text-sm font-bold mb-2">
                Password
              </label>
              {Password}
            </div>
            <div className="flex items-center justify-between">
              <button
                onClick={signup}
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                type="button"
              >
                Sign Up
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="rounded-b-lg bg-gray-800 ">
        <pre className="scrollbar-none m-0 p-0 ">
          <code className="inline-block text-white p-4 scrolling-touch !whitespace-pre-wrap">
            {res}
          </code>
        </pre>
      </div>
    </div>
  );
}
