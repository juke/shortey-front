"use client";
import { useEffect, useState } from "react";

export default function Login({ params }: { params: { short: string } }) {
  const [link, setLink] = useState<any>();
  const [res, setRes] = useState("");

  const getLink = async () => {
    try {
      const response = await fetch(
        `https://shortey.paas.juke.fr/link/${params.short}`
      );

      if (response.status !== 200) return setRes(await response.text());

      const { url } = await response.json();

      return {
        url, status: response.status
      };
    } catch (err: any) {
      setLink(err.message);
    }
  };

  const deleteLink = async (e: any) => {
    e.preventDefault();
    try {
      const response = await fetch(
        `https://shortey.paas.juke.fr/link/${params.short}`,
        {
          method: "DELETE",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      );

      if (response.status !== 204) return setRes(await response.text());

      return location.assign("/");
    } catch (err: any) {
      setRes(err.message);
    }
  };

  useEffect(() => {
    getLink().then((p: any) => {
      setLink(p)
    })
  }, []);

  if (link?.status === 200)
    return (
      <div>
        <div className="flex justify-center mt-6">
          <div className="w-full max-w-6xl">
            <div className="rounded-b-lg bg-white dark:bg-gray-900 p-4">
              <h2 className="text-4xl font-extrabold dark:text-white">{link?.title}</h2>
              <p className="my-4 font-bold text-lg text-gray-500">{link?.description}</p>
              <p className="my-4 text-lg text-gray-500">{link?.language}</p>
              <pre className={`scrollbar-none m-0 p-0 language-${link?.language}`}>
                <div className=" p-4 scrolling-touch whitespace-pre-wrap">
                  <a href={link.url}>{link.url}</a>
                </div>
                <div className=" p-4 scrolling-touch whitespace-pre-wrap">
                  <a href={`${window.location.origin}/l/${params.short}`}>{window.location.origin}/l/{params.short}</a>
                </div>
              </pre>
            </div>
          </div>
        </div>
        <div className="flex justify-center">
          <div className="w-full max-w-xl">
            <div className="bg-white dark:bg-gray-900 shadow-md rounded px-8 pt-6 pb-8 mb-4 mt-4">
              <div className="flex items-center justify-between">
                <button
                  onClick={deleteLink}
                  className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                  type="button"
                >
                  Delete
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="rounded-b-lg bg-gray-800 ">
          <pre className="scrollbar-none m-0 p-0 ">
            <code className="inline-block text-white p-4 scrolling-touch !whitespace-pre-wrap">
              {res}
            </code>
          </pre>
        </div>
      </div>
    );
  else {
    return (
      <div className="rounded-b-lg bg-gray-800 ">
        <pre className="scrollbar-none m-0 p-0 ">
          <code className="inline-block text-white p-4 scrolling-touch !whitespace-pre-wrap">
            {res}
          </code>
        </pre>
      </div>
    )
  }
}
