"use client";
import Link from "next/link";
import { useInput, useTextArea } from "../input";
import { useState } from "react";

export default function Home() {
  const [url, Url] = useInput("url");
  const [res, setRes] = useState("");

  const create = async (e: any) => {
    e.preventDefault();
    try {
      const response = await fetch("https://shortey.paas.juke.fr/link", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
        body: JSON.stringify({
          url,
        }),
      });

      if (response.status !== 201) return setRes(await response.text());

      const { short } = await response.json();
      if (short) return location.assign(`/link/${short}`);
      return setRes(await response.text());
    } catch (err: any) {
      setRes(err.message);
    }
  };

  return (
    <div>
      <div className="flex justify-center">
        <div className="w-full max-w-xl">
          <div className="bg-white dark:bg-gray-900 shadow-md rounded px-8 pt-6 pb-8 mb-4 mt-4">
            <div className="mb-4">
              <label className="block text-gray-700 dark:text-white text-sm font-bold mb-2">
                Url
              </label>
              {Url}
            </div>
            <div className="flex items-center justify-between">
              <button
                onClick={create}
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                type="button"
              >
                Create
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="rounded-b-lg bg-gray-800 ">
        <pre className="scrollbar-none m-0 p-0 ">
          <code className="inline-block text-white p-4 scrolling-touch !whitespace-pre-wrap">
            {res}
          </code>
        </pre>
      </div>
    </div>
  );
}
