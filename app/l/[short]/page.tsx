"use client";
import { useEffect, useState } from "react";

export default function Login({ params }: { params: { short: string } }) {
  const [link, setLink] = useState<any>();
  const [res, setRes] = useState("");

  const getPaste = async () => {
    try {
      const response = await fetch(
        `https://shortey.paas.juke.fr/link/${params.short}`
      );

      if (response.status !== 200) return setRes(await response.text());

      const { url } = await response.json();

      return {
        url, status: response.status
      };
    } catch (err: any) {
      setLink(err.message);
    }
  };

  useEffect(() => {
    getPaste().then((p: any) => {
      setLink(p)
      console.log(123, p)
      window.location.href = p.url
    })
  }, []);

  if (link?.status === 200)
    return (
      <div>
        Redirecting
      </div>
    );
  else {
    return (
      <div className="rounded-b-lg bg-gray-800 ">
        <pre className="scrollbar-none m-0 p-0 ">
          <code className="inline-block text-white p-4 scrolling-touch !whitespace-pre-wrap">
            {res}
          </code>
        </pre>
      </div>
    )
  }
}
