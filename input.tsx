import React, { useState } from "react";

export function useInput(type: string) {
  const [value, setValue] = useState("");
  const input = (
    <input
      value={value}
      onChange={(e) => setValue(e.target.value)}
      type={type}
      className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
    />
  );
  return [value, input];
}

export function useTextArea() {
  const [value, setValue] = useState("");
  const input = (
    <textarea
      value={value}
      onChange={(e) => setValue(e.target.value)}
      className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
    />
  );
  return [value, input];
}
